import React, {Component} from "react";
import {BrowserRouter as Router, Switch, Route, Link, withRouter, useParams, useHistory} from "react-router-dom";
import axios from 'axios';
import {observable} from 'mobx';
import {observer} from 'mobx-react';
import {Card, Menu, Button, Container, Message, Dropdown, Grid, Form, Header, Image} from 'semantic-ui-react';

function formatDate(date) {
  return date.toLocaleDateString();
}

const API_URL = 'http://5db050f78087400014d37dc5.mockapi.io/api/users/3/events';

export default
@observer
class App extends Component {
  @observable events = [];

  componentDidMount() {
    this.fetchEvents('dete');
  }

  fetchEvents = (sorting) => {
    axios.get(API_URL + '?sortBy=' + sorting)
      .then((response) => {
        this.events = response.data;
      })
      .catch((error) => {
        console.log(error);
      });
  }

  render() {
    return (
      <Router>
        <MainMenu /> 
  
        <Switch>
          <Route path="/add-event">
            <AddEventForm
              fetchEvents={this.fetchEvents}
            />
          </Route>
          <Route path="/event/:id">
            <Event
              events={this.events}
              fetchEvents={this.fetchEvents}
            />
          </Route>
          <Route path="/past">
            <Events
              events={this.events.filter((e) => {
                return new Date(e.dete) < new Date();
              })}
              fetchEvents={this.fetchEvents}
            />
          </Route>
          <Route path="/upcoming">
            <Events
              events={this.events.filter((e) => {
                return new Date(e.dete) > new Date();
              })}
              fetchEvents={this.fetchEvents}
            />
          </Route>
          <Route path="/">
            <Events
              events={this.events.filter((e) => {
                return formatDate(new Date(e.dete)) === formatDate(new Date());
              })}
              fetchEvents={this.fetchEvents}
            />
          </Route>
        </Switch>
      </Router>
    );
  }
}

@withRouter
class MainMenu extends Component {
  render() {
    const currentUrl = this.props.location.pathname;
    return (
      <Menu>
        <Menu.Item
          as={Link}
          to="/past"
          name='past'
          active={currentUrl === '/past'}
        >
          Прошедшие
        </Menu.Item>

        <Menu.Item
          as={Link}
          to="/"
          name='current'
          active={currentUrl === '/'}
        >
          Текущие
        </Menu.Item>

        <Menu.Item
          as={Link}
          to="/upcoming"
          name='upcoming'
          active={currentUrl === '/upcoming'}
        >
          Ближайшие
        </Menu.Item>
      </Menu>
    );
  }
}

class Events extends Component {
  deleteEvent = (eventId) => {
    axios.delete(API_URL + '/' + eventId)
      .then(() => {
        this.props.fetchEvents('dete');
      })
      .catch((error) => {
        console.log(error);
      });
  }

  render() {
    const sortingOptions = [
      {
        key: 'dete',
        text: 'Дате',
        value: 'dete',
      },
      {
        key: 'title',
        text: 'Названию',
        value: 'title',
      },
    ];
    return (
      <Container>
        <Grid style={{paddingLeft: '7px'}}>
          <Grid.Row columns={2}>
            <Grid.Column textAlign='left'>
              <Button as={Link} to="/add-event" positive>Добавить событие</Button>
            </Grid.Column>
            <Grid.Column textAlign='right'>
              <span>
                Сортировать по{' '}
                <Dropdown
                  inline
                  options={sortingOptions}
                  defaultValue={sortingOptions[0].value}
                  onChange={(e, data) => {
                    this.props.fetchEvents(data.value);
                  }}
                />
              </span>
            </Grid.Column>
          </Grid.Row>
        </Grid>
        {this.props.events.length > 0 ?
          <Card.Group itemsPerRow={1}>
            {this.props.events.map((event) => {
              return (
                <Card key={event.id}>
                  <Card.Content>
                    <Card.Header>
                      <Link to={'/event/' + event.id}>{event.title}</Link>
                    </Card.Header>
                    <Card.Meta>{'Дата публикации: ' + formatDate(new Date(event.dete))}</Card.Meta>
                    <Card.Meta>{'Комментарии: ' + event.comments.length}</Card.Meta>
                    <Card.Description>{event.description}</Card.Description>
                  </Card.Content>
                  <Card.Content extra textAlign='right'>
                    <Button
                      basic
                      color='red'
                      onClick={() => this.deleteEvent(event.id)}
                    >
                      Удалить
                    </Button>
                  </Card.Content>
                </Card>
              );
            })}
          </Card.Group>
        :
          <Message content='Нет событий' />
        }
      </Container>
    );
  }
}

function Event(props) {
  const params = useParams();
  const event = props.events.find((e) => {
    return e.id === params.id;
  });
  if (!event) return null;

  return (
    <Container>
      <Card.Group itemsPerRow={1}>
        <Card>
          <Card.Content>
            <Card.Header>{event.title}</Card.Header>
            <Card.Meta>{'Дата публикации: ' + formatDate(new Date(event.dete))}</Card.Meta>
            <Card.Description>{event.description}</Card.Description>
          </Card.Content>
        </Card>
      </Card.Group>
      <Header>Комментарии:</Header>
      {event.comments.length > 0 ?
        <Card.Group itemsPerRow={1}>
          {event.comments.map((comment) => {
            return (
              <Card>
                <Card.Content>
                  <Image
                    floated='left'
                    size='mini'
                    src={comment.avatar}
                  />
                  <Card.Header>{comment.name}</Card.Header>
                  <Card.Meta>{formatDate(new Date(comment.date))}</Card.Meta>
                  <Card.Description>{comment.text}</Card.Description>
                </Card.Content>
                <Card.Content extra textAlign='right'>
                  <Button
                    basic
                    color='red'
                    onClick={
                      () => {
                        event.comments = event.comments.filter((comm) => comm.id !== comment.id);
                        axios.put(API_URL + '/' + event.id, event)
                          .then(() => {
                            props.fetchEvents('dete');
                          })
                          .catch((error) => {
                            console.log(error);
                          });
                      }
                    }
                  >
                    Удалить
                  </Button>
                </Card.Content>
              </Card>
            );
          })}
        </Card.Group>
      :
        <Message>Комментарии отсутствуют.</Message>
      }
    </Container>
  );
}

@observer
class AddEventForm extends Component {
  @observable title = ''
  @observable description = ''
  @observable dete = ''

  render() {
    return (
      <Container>
        <Form>
          <Header>Создание мероприятия</Header>
          <Form.Field>
            <label>Заголовок:</label>
            <input
              placeholder='Заголовок'
              value={this.title}
              onChange={(e) => {
                this.title = e.target.value;
              }}
            />
          </Form.Field>
          <Form.Field>
            <label>Описание:</label>
            <input
              placeholder='Описание'
              value={this.description}
              onChange={(e) => {
                this.description = e.target.value;
              }}
            />
          </Form.Field>
          <Form.Field>
            <label>Дата:</label>
            <input
              placeholder='01.01.2020'
              value={this.dete}
              onChange={(e) => {
                this.dete = e.target.value;
              }}
            />
          </Form.Field>
          <SubmitButton
            data={{
              title: this.title,
              description: this.description,
              dete: this.dete,
            }}
            fetchEvents={this.props.fetchEvents}
          />
        </Form>
      </Container>
    );
  }
}

function SubmitButton(props) {
  const history = useHistory();
  return (
    <Button
      positive
      type='submit'
      onClick={() => {
        axios.post(API_URL, props.data)
          .then(() => {
            props.fetchEvents('dete');
            history.push('/');
          })
          .catch((error) => {
            console.log(error);
          });
      }}
    >
      Создать
    </Button>
  );
}